<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'hotel_saint_paulos' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'U8OHaXc6!a7P`SIyTKTC2]}[*WYWvMJY^gc.1/FFT?&TG}dt=2S&l~Xb:v}axSkN' );
define( 'SECURE_AUTH_KEY',  '-@nLkuiOI^#-w];#Ytv2uE^x=N>4H<D#Mu78dSlgFof{^7>R6F[U!T4<fnGa>b*f' );
define( 'LOGGED_IN_KEY',    'HS]Hu6D#|<=kii.ve7HjZXAT~G}h;o7Bp@-(IE4f_=JeB3[k(:^lxv&juP Dy[Y1' );
define( 'NONCE_KEY',        'N[M.L|+66PkHV[aekziw8I}XqM=(sl~l!y_a#fC!b+wp~|{qKg9!nhT-seVJuylx' );
define( 'AUTH_SALT',        'der-^7=UY&IvHTXV1uvG#6w<dKN(T^*h<]g9y~CV^f91D6a17u{=j1gh9`ZWKebj' );
define( 'SECURE_AUTH_SALT', 'I;ZwD$&IsnY&+a&=NeGYR(s+)w!r0/bLNWYQ03j9ofKUC9pKVeXyrYuz3OM]_iD-' );
define( 'LOGGED_IN_SALT',   '[2c:AI#!_ [_rPS?P3(#6s(Uc{Cc{b?fDu;E G}e`(}H{B_#70lVuML6djIN2r^x' );
define( 'NONCE_SALT',       '=_$(YQ&>O# d6O[2=)z<:hBfR2 7AyiM1e]eU>:+k*$e)QSU4c]mm3d_#CH(6IcF' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
