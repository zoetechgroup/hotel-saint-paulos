<?php
/**
 * Template file for footer area
 */
$footer_copyright = get_theme_mod('footer_copyright_text','<p>'.__( '<a href="https://wordpress.org">Proudly powered by WordPress</a> | Theme: <a href="https://spicethemes.com" rel="designer">SpicePress</a> by SpiceThemes', 'spicepress' ).'</p>');
?>
<!-- Footer Section -->
<footer class="site-footer">		
	<div class="container">
		
			<div class="row">
				<div class="col-md-6">
					<div style="color: white">
						<h3 class="text-success">A propos</h3>
						<b>L'Hôtel Saint Paulos</b> est le nouvel hôtel incontournable d'ADIDOGOME<br><br>
						Tout a été conçu, Architecture et Décors pour le seul but de rendre le plus
						agréable possible le séjour de nos convives. Venez, vous ne serez pas déçus<br><br>
						<h3 class="text-success">
							Suivez-nous sur Facebook<br><br>
						</h3>
					</div>
				</div>
				<div class="col-md-6">
					<div style="color: white">
						<h3 class="text-success">Adresse</h3>
							<b>Quartier ADIDOGOME</b><br>
							2ème Rue derrière le CEG D'ADIDOGOME ou en face du commissariat 9èm Arrondissement
							Lomé - TOGO<br>
							Mobile: (00228) 90 09 94 27<br>Fixe: (00228) 22 50 02 54<br>Email: 
							saintpaulos2@yahoo.fr
					</div>
				</div>
			<div class="col-md-12">
					<div class="site-info wow fadeIn animated" data-wow-delay="0.4s">
						<div>
							© 2019 Zoe Création Tous droits reservés
						</div>
					</div>
				</div>			
			</div>	
	
		
	</div>
</footer>
<!-- /Footer Section -->
<div class="clearfix"></div>
</div><!--Close of wrapper-->
<!--Scroll To Top--> 
<a href="#" class="hc_scrollup"><i class="fa fa-chevron-up"></i></a>
<!--/Scroll To Top--> 
<?php wp_footer(); ?>
</body>
</html>